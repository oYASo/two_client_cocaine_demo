#include <cocaine/framework/services/app.hpp>
#include <cocaine/framework/services/storage.hpp>
// std
#include <iostream>
//==============================================================================
namespace cf = cocaine::framework;
//==============================================================================
void handler(cf::future<std::string>& f)
{
  try
  {
    std::cout << "Compute done: "
              << cf::unpack<std::string>(f.get())
              << std::endl;
  }
  catch (const std::exception& e)
  {
    std::cout << "Error: " << e.what() << std::endl;
  }
}
//==============================================================================
int main(int argc, char* argv[])
{
  auto manager =
      cf::service_manager_t::create(
        cf::service_manager_t::endpoint_t("192.168.0.76", 10053));

  auto app = manager->get_service<cf::app_service_t>("two_service");

  for (int i = 0; i < 5000; ++i)
  {
    auto message = "test" + std::to_string(i);
    auto g = app->enqueue("compute", message);
//    g.map(&handler);
  }

  return 0;
}

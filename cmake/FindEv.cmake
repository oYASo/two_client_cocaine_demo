find_library(LIBEV_LIBRARIES NAMES ev)
find_path(LIBEV_INCLUDE_DIR ev.h PATH_SUFFIXES include/ev include)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  libev DEFAULT_MSG LIBEV_LIBRARIES LIBEV_INCLUDE_DIR)
#include "service.hpp"
// std
#include <thread>
//==============================================================================
namespace cf = cocaine::framework;
//==============================================================================
int main(int argc, char* argv[])
{
  return cocaine::framework::run<Service>(argc, argv);
}
//==============================================================================
Service::ComputeEvent::
ComputeEvent(Service& s)
  : cocaine::framework::handler<Service>(s)
{
}
//==============================================================================
void Service::ComputeEvent::
on_chunk(const char* chunk, size_t size)
{
  auto message = cf::unpack<std::string>(chunk, size);
  std::thread(
        &Service::ComputeEvent::compute, shared_from_this(), message).detach();
}
//==============================================================================
void Service::ComputeEvent::
compute(const std::string& message)
{
  COCAINE_LOG_INFO(parent().mLogger, "Start compute... %s", message);
  std::this_thread::sleep_for(std::chrono::seconds(10));
  COCAINE_LOG_INFO(parent().mLogger, "Compute done!");
}
//==============================================================================
Service::
Service(cocaine::framework::dispatch_t& d)
  : mLogger(d.service_manager()->get_system_logger())
{
  d.on<ComputeEvent>("compute", *this);
}
//==============================================================================

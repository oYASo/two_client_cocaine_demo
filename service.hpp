#pragma once
// cocaine
#include <cocaine/framework/dispatch.hpp>
#include <cocaine/framework/services/storage.hpp>
// std
#include <memory>
#include <string>
#include <vector>
//==============================================================================
class Service
{
private:
  struct ComputeEvent : public cocaine::framework::handler<Service>
                      , public std::enable_shared_from_this<ComputeEvent>
  {
    ComputeEvent(Service& s);
    void on_chunk(const char* chunk, size_t size);

    void compute(const std::string& message);
  };

public:
  explicit Service(cocaine::framework::dispatch_t& d);

private:
  std::shared_ptr<cocaine::framework::logger_t> mLogger;
};
